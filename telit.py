import serial
import time
from timeit import default_timer as getTime
import numpy as np
from matplotlib import pyplot as plt

# Modem config
modem_port = serial.Serial()
modem_port.baudrate = 115200
modem_port.port = "COM13" #change for different computers
modem_port.timeout = 0 #increase timeout in case of bad reception
modem_port.open()

txTime= []
thput= []
latencyT=[]
thputS= []
rxTime=[]
cnctTime= []
dcnctTime=[]

def send_at_command(command):
    modem_port.reset_output_buffer()
    modem_port.reset_input_buffer()
    while modem_port.readline():
        pass
    command=command+"\r"
    modem_port.write(command.encode('ascii'))
    modem_port.sendBreak() #may need to uncomment this line for windows

def read_command_response():
    _res=""
    response = modem_port.readline().decode('ascii').strip()
    while not (("OK" in response) or ("ERROR" in response) or ("CARRIER" in response) or (">" in response)):
        response += modem_port.readline().decode('ascii').strip()

    return response

if modem_port.isOpen():

    i = 0
    while(i < 100):
        timestr = time.strftime("%Y%m%d-%H%M%S")
        f=open(timestr+"telit.txt", "a+")
        

        t1 = getTime()
        send_at_command("AT")
        response = read_command_response()
        t2 = getTime()
        duration =  t2-t1
        print(response)
        print(duration)

        print("<< Name of manufacturer >> ")
        send_at_command("AT+CGMI")
        response = read_command_response()
        print(response)
        f.write("<< Name of manufacturer >>")
        f.write(response)
        f.write("\n")

        print("<< Model number >>  ")
        send_at_command("AT+CGMM")
        response = read_command_response()
        print(response)
        f.write("<< Model number >> ")
        f.write(response)
        f.write("\n")

        print("<< IMEI number  >>  ")
        send_at_command("AT+CGSN")
        response = read_command_response()
        print(response)
        f.write("<< IMEI number  >> ")
        f.write(response)
        f.write("\n")
 
        print("<< Software version >>   ")
        send_at_command("AT+CGMR")
        response = read_command_response()
        print(response)
        f.write("<< Software version >>  ")
        f.write(response)
        f.write("\n")
 
        print("<< IMSI number >>  ")
        send_at_command("AT+CIMI")
        response = read_command_response()
        print(response)
        f.write("<< IMSI number >>  ")
        f.write(response)
        f.write("\n")


 
        print("<<  get the current network status >>   ")
        send_at_command("AT#RFSTS")
        response = read_command_response()
        print(response)
        f.write("<<  get the current network status >>  ")
        f.write(response)
        f.write("\n")


        # Ready module
        print("<< received signal strength (<rssi>) and quality (<ber>) >> ")
        t1 = getTime()
        send_at_command("AT+CSQ")
        response = read_command_response()  
        t2 = getTime()
        duration =  t2-t1
        print(response)
        print("\n")
        print(duration)
        throughput= (len(response)*8)/duration
        print(throughput)      
        f.write("received signal strength (<rssi>) and quality (<ber>)")
        f.write(response)
        f.write("\n")

        # Ready module
        send_at_command("AT+CREG=1")
        response = read_command_response()
        print(response)
        print("\n")
        f.write(response)
        f.write("\n")
        
        send_at_command("AT#CGPADDR=1")
        response = read_command_response()
        print(response)
        print("\n")
        f.write(response)
        f.write("\n")
        
        # if("\"" not in response):
        send_at_command("AT#SGACT=1,1")
        response = read_command_response()
        print(response)
        print("\n")
        f.write(response)
        f.write("\n")

        # send_at_command("AT#SD=1,0,50030,\"184.72.228.61\",0,0,1")
        t1 = getTime()
        send_at_command("AT#SD=1,0,10510,\"modules.telit.com\",0,0,1")
        response = read_command_response()
        t2 = getTime()
        duration =  t2-t1
        cnctTime.append(duration)
        print(response)
        print("\n")
        print("Time required for conncetion: ", duration, "\n")
        tex= "Time required for conncetion: "+str(duration)+"\n"
        f.write(response)
        f.write("\n")
        f.write(tex)

        t1 = getTime()
        send_at_command("AT#SSEND=1")
        response1 = read_command_response()
        print(response1)
        print("\n")
        f.write(response1)
        f.write("\n")
        datatoSend="Hello! Sent from python script , Go to the settings to connect to the COM of the modem by viewing the options under COM Port and changing the Baud rate based on the supported one by the modem (by default it is 9600). iteration :" + str(i+1)
        send_at_command(datatoSend + "\032")
        response2 = read_command_response()
        t2 = getTime()
        duration =  t2-t1
        txtm=duration
        txTime.append(duration)
        print(response2)
        print("\n")
        print("Time required for send data until reaching ctrl+z: ", duration, "\n")
        tex = "Time required for send data until reaching ctrl+z: "+str(duration)+"\n"
        f.write(response2)
        f.write("\n")
        f.write(tex)
        time.sleep(3)

        t1 = getTime()
        send_at_command("AT#SRECV=1,240")
        response = read_command_response()
        t2 = getTime()
        duration =  t2-t1
        rxtm= duration
        rxTime.append(duration)
        latencyT.append((rxtm+txtm))
        print(response)
        print("\n")
        thrput = (len(datatoSend)*8)/(rxtm+txtm)
        thputS.append(thrput)
        tex= "Total Network Throughput : "+str(thrput)+" bit/sec \n"
        f.write(response)
        f.write("\n")
        print(tex)
        f.write(tex)

        tex= "Total Network Latency : "+str((rxtm+txtm))+" msec \n"
        print(tex)
        f.write(tex)

        throughput= (len(response)*8)/duration
        thput.append(throughput)        
        tex= "=Throughput for the received data: "+str(throughput)+" bit/sec \n"
        print(tex)
        f.write(tex)
        print("Time required for recieve data: ", duration , "\n")
        tex = "Time required for recieve data: "+str(duration)+"\n"
        f.write(tex)
        f.write("\n")

        #close connection
        t1 = getTime()
        send_at_command("AT#SH=1")
        response = read_command_response()
        t2 = getTime()
        duration =  t2-t1
        dcnctTime.append(duration)
        print(response)
        print("\n")
        print("Time required to close connection: ", duration, "\n")
        tex = "Time required to close connection: "+str(duration)+"\n"
        f.write(response)
        f.write("\n")
        f.write(tex)


        send_at_command("AT#SGACT=1,0")
        response = read_command_response()
        print(response)
        print("\n")
        f.write(response)
        f.write("\n")
        time.sleep(2)
        i += 1
        f.close()

modem_port.close()


timestr = time.strftime("%Y%m%d-%H%M%S")
with open(timestr+"CNCT.txt", 'w') as f:
    for item in cnctTime:
        f.write("%f\n" % item)

with open(timestr+"THPUT.txt", 'w') as f:
    for item in thput:
        f.write("%f\n" % item)

with open(timestr+"THPUTOTAL.txt", 'w') as f:
    for item in thputS:
        f.write("%f\n" % item)

with open(timestr+"DCNCT.txt", 'w') as f:
    for item in dcnctTime:
        f.write("%f\n" % item)

with open(timestr+"TX.txt", 'w') as f:
    for item in txTime:
        f.write("%f\n" % item)


with open(timestr+"RX.txt", 'w') as f:
    for item in rxTime:
        f.write("%f\n" % item)

thputNp= np.asarray(thput)
thputSNp= np.asarray(thputS)
txTimeNp= np.asarray(txTime)
latenTimeNp= np.asarray(latencyT)
rxTimeNp= np.asarray(rxTime)
cnctTimeNp= np.asarray(cnctTime)
dcnctTimeNp= np.asarray(dcnctTime)


print("\tAverage Throughput for send data: {:.3f} bit per sec".format(thputNp.mean()))
print("\tMin Throughput for send data: {:.3f} bit per sec".format(thputNp.min()))
print("\tMax Throughput for send data: {:.3f} bit per sec".format(thputNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage TOTAL Throughput: {:.3f} bit per sec".format(thputSNp.mean()))
print("\tMin TOTAL Throughput: {:.3f} bit per sec".format(thputSNp.min()))
print("\tMax TOTAL Throughput: {:.3f} bit per sec".format(thputSNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage Latency: {:.3f} sec".format(latenTimeNp.mean()))
print("\tMin Latency: {:.3f} sec".format(latenTimeNp.min()))
print("\tMax Latency: {:.3f} sec".format(latenTimeNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage Time for seting up Conncetion: {:.3f} sec".format(cnctTimeNp.mean()))
print("\tMin Time for seting up Conncetion: {:.3f} sec".format(cnctTimeNp.min()))
print("\tMax Time for seting up Conncetion: {:.3f} sec".format(cnctTimeNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage Time for closing connection: {:.3f} sec".format(dcnctTimeNp.mean()))
print("\tMin Time for closing connection: {:.3f} sec".format(dcnctTimeNp.min()))
print("\tMax Time for closing connection: {:.3f} sec".format(dcnctTimeNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage Time for sending data: {:.3f} sec".format(txTimeNp.mean()))
print("\tMin Time for sending data: {:.3f} sec".format(txTimeNp.min()))
print("\tMax Time for sending data: {:.3f} sec".format(txTimeNp.max()))
print(">>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<")
print("\tAverage Time for recieving data: {:.3f} sec".format(rxTimeNp.mean()))
print("\tMin Time for recieving data: {:.3f} sec".format(rxTimeNp.min()))
print("\tMax Time for recieving data: {:.3f} sec".format(rxTimeNp.max()))

plt.plot(thputNp)
plt.title("Network THROUGHPUT")
plt.savefig(timestr+'THPUT.png')
plt.show()

plt.plot(thputSNp)
plt.title("Network Total THROUGHPUT")
plt.savefig(timestr+'THPUTtotal.png')
plt.show()


plt.plot(latenTimeNp)
plt.title("Network Latency")
plt.savefig(timestr+'Latency.png')
plt.show()

fig, axs = plt.subplots(2)
axs[0].plot(cnctTimeNp)
axs[1].plot(dcnctTimeNp)
axs[0].set_title('Time for seting up the conncetion')
axs[1].set_title('Time for closing connection')
plt.savefig(timestr+'PDPres.png')
plt.show()


fig, axs = plt.subplots(2)
axs[0].plot(txTimeNp)
axs[1].plot(rxTimeNp)
axs[0].set_title('Time for sending')
axs[1].set_title('Time for recieving')
plt.savefig(timestr+'TXRX.png')
plt.show()
